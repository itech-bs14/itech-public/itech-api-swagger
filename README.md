# Flask-RESTx API Project

## Project Description
This project provides a simple REST API built with Flask and Flask-RESTx. It includes examples for HTML rendering and API endpoints for handling messages. The API documentation is automatically generated using Swagger, making it easy to use and integrate.

## Prerequisites
* Python 3.x
* pip (Python Package Installer)

## Installation

1. **Clone the project**:
   ```bash
   git clone https://github.com/your-repository-url
   cd your-repository-name
   ```

2. **Install dependencies**:
   ```bash
   pip install -r requirements.txt
   ```

3. **Start the Flask service locally**:
   ```bash
   export FLASK_APP=app.py
   export FLASK_ENV=development
   flask run
   ```
   The service will run by default at `http://127.0.0.1:5000`.

## Flask-RESTx and Swagger Documentation

### API Endpoints & Flask-RESTx
Flask-RESTx is an extension for Flask that adds support for quickly building REST APIs. It encourages best practices with minimal setup. If you're familiar with Flask, Flask-RESTx should be easy to pick up. It provides a coherent collection of decorators and tools to describe your API and properly expose its documentation (using Swagger).

### Swagger
Flask-RESTx seamlessly integrates with Swagger, a powerful tool for API documentation. Swagger allows you to visually display and directly test API endpoints, making development and integration much easier.

#### Accessing the API Documentation
Once the Flask service is running, you can access the automatically generated Swagger documentation at the following URL:
```
http://127.0.0.1:5000/
```
This documentation will show all available endpoints and allow you to test them directly.

[Learn more about Flask-RESTx](https://flask-restx.readthedocs.io/en/latest/)

## Web Server Gateway Interface (WSGI)

### Gunicorn
We use `Gunicorn` as the WSGI server for production. Gunicorn is a Python WSGI HTTP server that is quick and easy to set up while providing high performance and reliability.

[Learn more about Gunicorn](https://gunicorn.org/)

## Development and Collaboration

### Technologies Used
- **Python**: Programming language for implementation.
- **Flask**: Microframework for web development.
- **Flask-RESTx**: Extension for building APIs.
- **GitLab**: Platform for version control and CI/CD.

## Important Links
- [Flask-RESTx Documentation](https://flask-restx.readthedocs.io/en/latest/)
- [Flask Introduction](https://flask.palletsprojects.com/en/latest/)
- [Gunicorn Documentation](https://gunicorn.org/)